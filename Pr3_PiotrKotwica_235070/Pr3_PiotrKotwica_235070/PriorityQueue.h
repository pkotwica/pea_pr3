#pragma once
#include <iostream>
#include "TSPSolution.h"

using namespace std;

class PriorityQueue
{
	struct elem{
		TSPSolution* key = NULL;
		int value = 0;
		elem* next = NULL;
	};

	elem* head;
	TSPSolution* bestElemEver;


public:
	PriorityQueue(TSPSolution* bestElemEver);
	~PriorityQueue();

	void add(TSPSolution* x, int** matrix);
	TSPSolution* get(int index);
	int getAmountOfElements();
};

