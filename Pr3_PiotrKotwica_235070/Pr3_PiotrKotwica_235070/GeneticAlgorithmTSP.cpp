#include "pch.h"
#include "GeneticAlgorithmTSP.h"


GeneticAlgorithmTSP::GeneticAlgorithmTSP(int** miasto, int liczbaMiast, int kryterium_stopu, int amountOfPopulation, float wspolczynnikMutacji, float wspolczynnikKrzyzownaia, int metodaMutacji)
{
	this->miasto = miasto;
	this->liczbaMiast = liczbaMiast;
	this->kryterium_stopu = kryterium_stopu;
	this->amountOfPopulation = amountOfPopulation;
	this->wspolczynnikMutacji = wspolczynnikKrzyzownaia;
	this->wspolczynnikKrzyzownaia = wspolczynnikMutacji;
	this->metodaMutacji = metodaMutacji;

	population = new Population(amountOfPopulation, wspolczynnikMutacji, wspolczynnikKrzyzownaia, miasto, liczbaMiast, metodaMutacji);
}

GeneticAlgorithmTSP::~GeneticAlgorithmTSP()
{
	if (population != NULL)
		delete population;
}

TSPSolution * GeneticAlgorithmTSP::searchSolution()
{
	//TSPSolution* bufor = NULL;
	int buforOptimumSolution;
	TSPSolution* optimum = NULL;
	int optimumSolution;
	Timer* timer = new Timer();

	//population->generateNewPopulation();
	//optimum = population->findBestIndividual();
	//optimumSolution = optimum->calculateSolution(miasto);

	timer->start();
	do {
		
		population->generateNewPopulation();
		//bufor = population->findBestIndividual();
		//buforOptimumSolution = bufor->calculateSolution(miasto);

		//if (optimumSolution > buforOptimumSolution) {
		//	optimum = bufor;
		//	optimumSolution = buforOptimumSolution;
		//}

	} while (!timer->stop(kryterium_stopu));


	delete timer;

	return population->findBestIndividual();
	//return optimum;
}


