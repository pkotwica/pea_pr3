#include "pch.h"
#include "PriorityQueue.h"


PriorityQueue::PriorityQueue(TSPSolution * bestElemEver)
{
	head = NULL;
	this->bestElemEver = bestElemEver;
}

PriorityQueue::~PriorityQueue()
{
	elem* next = head;

	while (head != NULL) {
		next = head->next;

		//if (head->key != bestElemEver) {
			delete head->key;					//problem z dostepem do pamieci
			delete head;
		//}
			

		head = next;
	}
}

void PriorityQueue::add(TSPSolution * x, int** matrix)
{
	if (head == NULL) {
		head = new elem();
		head->key = x;
		head->value = x->calculateSolution(matrix);
	}
	else {
		elem* ptr = head;
		elem* newElem = new elem();
		newElem->value = x->calculateSolution(matrix);
		newElem->key = x;

		if (ptr->value < newElem->value) {

			while (ptr->next != NULL && ptr->next->value < newElem->value)
				ptr = ptr->next;

			newElem->next = ptr->next;
			ptr->next = newElem;
		}
		else
		{
			head = newElem;
			head->next = ptr;
		}
	}

	if (bestElemEver == NULL)
		bestElemEver = head->key;
	else
		if (bestElemEver->calculateSolution(matrix) > head->value)
			bestElemEver = head->key;
}

TSPSolution * PriorityQueue::get(int index)
{
	int indexer = 0;
	elem* ptr = head;

	while (indexer != index && ptr->next != NULL) {
		ptr = ptr->next;
		indexer++;
	}

	return ptr->key;
}

int PriorityQueue::getAmountOfElements()
{
	elem* ptr = head;
	int counter = 0;

	while (ptr != NULL) {
		counter++;
		ptr = ptr->next;
	}
	
	return counter;
}