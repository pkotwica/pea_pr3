#include "pch.h"
#include "Population.h"



Population::Population(int amountOfPopulation, float wspolczynnikMutacji, float wspolczynnikKrzyzownaia, int** matrix, int amountOfMatrix, int metodaMutacji)
{
	//bestSolution = new TSPSolution(amountOfMatrix);
	bestSolution = NULL;

	this->amountOfPopulation = amountOfPopulation;
	this->wspolczynnikMutacji = wspolczynnikMutacji;
	this->wspolczynnikKrzyzownaia = wspolczynnikKrzyzownaia;
	this->matrix = matrix;
	this->amountOfMatrix = amountOfMatrix;
	this->metodaMutacji = metodaMutacji;

	queue = NULL;
}

Population::~Population()
{
	if (queue != NULL) {
		delete queue;
		queue = NULL;
	}
}

void Population::generateNewPopulation()
{
	if (queue == NULL) {
		queue = new PriorityQueue(NULL);				//NULL - bo brak dotychczasowego najlepzego rozwiazania

		for (int i = 0; i < amountOfPopulation; i++) {
			TSPSolution* newSolution = new TSPSolution(amountOfMatrix);
			newSolution->generateRandomSoltion();
			queue->add(newSolution, matrix);
		}

		bestSolution = new TSPSolution(queue->get(0)->getTab(), amountOfMatrix);
	}
	else {
		PriorityQueue* newQueue = new PriorityQueue(bestSolution);
		int randomElem;

		for (int i = 0; i < (int)floor(amountOfPopulation / 2); i++) {
			do {
				randomElem = randomNumber(0, amountOfPopulation - 1);
			} while (i == randomElem);

			cross(queue->get(i), queue->get(randomElem), newQueue);
		}


		for (int i = 0; i < amountOfPopulation; i++) {
			if (newQueue->getAmountOfElements() >= amountOfPopulation)
				break;

			newQueue->add(new TSPSolution(queue->get(i)->getTab(), amountOfMatrix), matrix);
		}

		if (bestSolution->calculateSolution(matrix) > queue->get(0)->calculateSolution(matrix)) {
			delete bestSolution;
			bestSolution = new TSPSolution(queue->get(0)->getTab(), amountOfMatrix);
		}

		delete queue;
		queue = newQueue;
	}
}

TSPSolution * Population::findBestIndividual()
{
	return bestSolution;
}



void Population::cross(TSPSolution* firstParent, TSPSolution* secondParent, PriorityQueue* queue)
{
	if (execute(wspolczynnikKrzyzownaia)) {
		int startIndexOfSubTab = (int)floor(amountOfMatrix / 4);
		int endIndexOfSubTab = amountOfMatrix - ((int)floor(amountOfMatrix / 4) + 1);
		int sizeOfSubTab = (endIndexOfSubTab - startIndexOfSubTab) + 1;
		int* tabFirstParent = firstParent->getTab();
		int* tabSecondParent = secondParent->getTab();
		int* tabFirstChild = new int[amountOfMatrix];
		copyTab(tabFirstChild, tabSecondParent);
		int* tabSecondChild = new int[amountOfMatrix];
		copyTab(tabSecondChild, tabFirstParent);


		int* tempTab = new int[amountOfMatrix];

		copyTab(tempTab, tabFirstParent);

		for (int i = startIndexOfSubTab; i <= endIndexOfSubTab; i++)
			throwElemOfTab(tempTab, tabFirstChild[i]);

		for (int i = endIndexOfSubTab + 1; i < amountOfMatrix; i++)
			tabFirstChild[i] = tempTab[i - sizeOfSubTab];

		for (int i = 1; i < startIndexOfSubTab; i++)
			tabFirstChild[i] = tempTab[i];



		copyTab(tempTab, tabSecondParent);

		for (int i = startIndexOfSubTab; i <= endIndexOfSubTab; i++)
			throwElemOfTab(tempTab, tabSecondChild[i]);

		for (int i = endIndexOfSubTab + 1; i < amountOfMatrix; i++)
			tabSecondChild[i] = tempTab[i - sizeOfSubTab];

		for (int i = 1; i < startIndexOfSubTab; i++)
			tabSecondChild[i] = tempTab[i];

		if (metodaMutacji == 1) {
			mutateTrasnposition(tabFirstChild);
			mutateTrasnposition(tabSecondChild);
		}
		else {
			mutateInversion(tabFirstChild);
			mutateInversion(tabSecondChild);
		}

		queue->add(new TSPSolution(tabFirstChild, amountOfMatrix), matrix);
		queue->add(new TSPSolution(tabSecondChild, amountOfMatrix), matrix);

		delete[]tempTab;
		delete[]tabFirstChild;
		delete[]tabSecondChild;
	}
}

void Population::mutateTrasnposition(int* solution)
{
	if (execute(wspolczynnikMutacji)) {
		int bufor;
		int random1;
		int random2;

		do {
			random1 = randomNumber(1, amountOfMatrix - 1);
			random2 = randomNumber(1, amountOfMatrix - 1);
		} while (random1 == random2);


		bufor = solution[random1];
		solution[random1] = solution[random2];
		solution[random2] = bufor;
	}
}

void Population::mutateInversion(int * solution)
{
	if (execute(wspolczynnikMutacji)) {

		int startIndexOfSubTab = (int)floor(amountOfMatrix / 4);
		int endIndexOfSubTab = amountOfMatrix - ((int)floor(amountOfMatrix / 4) + 1);
		int sizeOfSubTab = (endIndexOfSubTab - startIndexOfSubTab) + 1;
		int* tabBufor = new int[amountOfMatrix];

		for (int i = 0; i < amountOfMatrix; i++)
			cout << solution[i] << "  ";
		cout << endl;


		for (int i = startIndexOfSubTab; i <= endIndexOfSubTab; i++)
			tabBufor[i] = solution[i];


		int j = startIndexOfSubTab;
		for (int i = endIndexOfSubTab; i >= startIndexOfSubTab; i--) {
			solution[j] = tabBufor[i];
			j++;
		}


		for (int i = 0; i < amountOfMatrix; i++)
			cout << solution[i] << "  ";
		cout << endl;

		delete tabBufor;
	}
}

bool Population::execute(float probability)
{
	int rand1 = randomNumber(1, 100);
	int rand2 = randomNumber(1, 100);
	float insideProb;

	if (rand1 > rand2)
		insideProb = ((float)rand2) / ((float)rand1);
	else
		insideProb = ((float)rand1) / ((float)rand2);

	if (insideProb <= probability)
		return true;
	else
		return false;
}

int Population::randomNumber(int from, int to)
{
	random_device rd; // non-deterministic generator
	mt19937 gen(rd()); // random engine seeded with rd()
	uniform_int_distribution<> dist(from, to); // distribute results between
	 // 1 and 1000000 inclusive

	return dist(gen);
}

void Population::copyTab(int * tab, int * copiedTab)
{
	for (int i = 0; i < amountOfMatrix; i++)
		tab[i] = copiedTab[i];
}

void Population::throwElemOfTab(int * tab, int value)
{
	for (int i = 0; i < amountOfMatrix; i++)
		if (tab[i] == value) {
			for (int j = i; j < amountOfMatrix - 1; j++)
				tab[j] = tab[j+1];
			break;
		}
}
