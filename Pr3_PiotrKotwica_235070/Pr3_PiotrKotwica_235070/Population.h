#pragma once
#include <iostream>
#include <random>
#include <cmath>
#include "TSPSolution.h"
#include "PriorityQueue.h"


using namespace std;

class Population
{
	TSPSolution* bestSolution;
	int amountOfPopulation;
	float wspolczynnikMutacji;
	float wspolczynnikKrzyzownaia;
	int** matrix;
	int amountOfMatrix;
	int metodaMutacji;

	PriorityQueue* queue;

public:
	Population(int amountOfPopulation, float wspolczynnikMutacji, float wspolczynnikKrzyzownaia, int** matrix, int amountOfMtrix, int metodaMutacji);
	~Population();


	void generateNewPopulation();
	TSPSolution* findBestIndividual();
	
private:
	void cross(TSPSolution* firstParent, TSPSolution* secondParent, PriorityQueue* queue);
	void mutateTrasnposition(int* solution);
	void mutateInversion(int* solution);
	bool execute(float probability);
	int randomNumber(int from, int to);
	void copyTab(int* tab, int* copiedTab);
	void throwElemOfTab(int* tab, int value);
};

