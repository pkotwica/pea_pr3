#include "pch.h"
#include "SprawozdanieMenu.h"


SprawozdanieMenu::SprawozdanieMenu()
{
	miasto = NULL;
	liczbaMiast = 0;
	kryterium_stopu = 5;
	dywersyfikacja = false;
	minimumOptimum = 0;
}


SprawozdanieMenu::~SprawozdanieMenu()
{
	resetCity();
}

void SprawozdanieMenu::resetCity()
{
	if (liczbaMiast != 0 && miasto != NULL)
	{
		for (int i = 0; i < liczbaMiast; i++)
			delete[] miasto[i]; //uwolnienie pamieci
		delete[] miasto; //uwolnienie pamieci

		miasto = NULL;
		liczbaMiast = 0;
	}
}

void SprawozdanieMenu::menu()
{
	int decyzja;
	bool czyKoniec = false;
	string filename;

	while (!czyKoniec)
	{
		system("cls");
		cout << "PROJEKT 3 - PROBLEM KOMIWOJAZERA (TSP)" << endl;
		cout << "MENU DO SPRAWOZDANIA" << endl;
		cout << "______________________________________" << endl << endl;

		cout << "Wybierz opcje" << endl;
		cout << "1 - Wczytaj dane z pliku" << endl;
		cout << "2 - Wprowadz kryterium stopu (Ustwione: " << kryterium_stopu << " s)" << endl;
		cout << "3 - Dywersywikacja ";
		if (dywersyfikacja == true)
			cout << "(wlaczona)" << endl;
		else
			cout << "(wylaczona)" << endl;
		cout << "4 - Testuj algorytm Symulownego Wyzarzania" << endl;
		cout << "5 - Testuj algorytm Tabu Search" << endl;
		cout << "6 - WYJDZ" << endl;
		cout << "Podaj numer: ";
		cin >> decyzja;

		do
		{
			if (czyPoprawnaDecyzja(8, decyzja))
				break;

			cout << "Zly numer!" << endl;
			cout << "Podaj numer: ";
			cin >> decyzja;

		} while (!czyPoprawnaDecyzja(8, decyzja));


		switch (decyzja)
		{
		case 1:
			cout << "Podaj nazwe pliku: ";
			cin >> filename;
			wczytajZPliku(filename);
			cout << "\nPlik zostal wczytany.\n";
			system("pause");
			break;
		case 2:
			cout << endl << "Podaj czas dla kryterium stopu (sekundy): ";
			cin >> kryterium_stopu;
			break;
		case 3:
			if (dywersyfikacja == true)
				dywersyfikacja = false;
			else
				dywersyfikacja = true;
			break;
		case 4:
			symulawaneWyzarzanie();
			break;
		case 5:
			tabuSerach();
			break;
		case 6:
			czyKoniec = true;
			break;
		}
	}
}

void SprawozdanieMenu::wczytajZPliku(string file_name)
{
	ifstream file;
	file.open(file_name.c_str());

	if (file.is_open())
	{
		resetCity();
		if (file_read_line(file, liczbaMiast))
		{
			miasto = new int *[liczbaMiast];			//tworzenie odpowiednio duzej macierzy
			for (int i = 0; i < liczbaMiast; i++)
				miasto[i] = new int[liczbaMiast];

			int *tab = new int[liczbaMiast];

			for (int i = 0; i < liczbaMiast; i++)
				if (file_read_line(file, tab, liczbaMiast))
				{
					for (int j = 0; j < liczbaMiast; j++)
						miasto[i][j] = tab[j];
				}
				else
				{
					cout << "File error - READ EDGE" << endl;
					break;
				}

			delete[] tab;
		}
		else
			cout << "File error - READ INFO" << endl;
		file.close();
	}
	else
		cout << "File error - OPEN" << endl;
	cout << endl << endl << "Podaj najlepsze rozwiazanie: ";
	cin >> minimumOptimum;
}




bool SprawozdanieMenu::czyPoprawnaDecyzja(int max, int decyzja)
{
	if (decyzja > max || decyzja < 1)
		return false;
	else
		return true;
}

void SprawozdanieMenu::symulawaneWyzarzanie()
{
	/*
	int iterations;
	int waga;
	double bladWzgledny = 0;

	cout << "Podaj ilosc powtorzen: ";
	cin >> iterations;

	for (int i = 0; i < iterations; i++) {

		SimulatedAnnealing* simulatedAnnealing = new SimulatedAnnealing(miasto, liczbaMiast);
		TSPSolution* solution;

		solution = simulatedAnnealing->solveTSP(kryterium_stopu);

		waga = solution->calculateSolution(miasto);



		bladWzgledny += abs((double)(waga - minimumOptimum) / (double)minimumOptimum);


		delete solution;
		delete simulatedAnnealing;
	}

	bladWzgledny = bladWzgledny / (double)iterations;

	cout << endl << "Sredni blad wzgledny: " << bladWzgledny << endl << endl;

	system("pause");
	system("pause");
	*/
}

void SprawozdanieMenu::tabuSerach()
{

	/*
	int iterations;
	int waga;
	double bladWzgledny = 0;

	cout << "Podaj ilosc powtorzen: ";
	cin >> iterations;

	for (int i = 0; i < iterations; i++) {

		TabuSearch* tabuSearch = new TabuSearch(miasto, liczbaMiast);
		TSPSolution* solution;

		solution = tabuSearch->solveTSP(kryterium_stopu, dywersyfikacja);

		waga = solution->calculateSolution(miasto);



		bladWzgledny += abs((double)(waga - minimumOptimum) / (double)minimumOptimum);


		delete solution;
		delete tabuSearch;
	}

	bladWzgledny = bladWzgledny / (double)iterations;

	cout << endl << "Sredni blad wzgledny: " << bladWzgledny << endl << endl;

	system("pause");
	system("pause");
	*/
}

bool SprawozdanieMenu::file_read_line(ifstream & file, int tab[], int size)
{
	string s;
	getline(file, s);

	if (file.fail() || s.empty())
		return(false);

	istringstream in_ss(s);

	for (int i = 0; i < size; i++)
	{
		in_ss >> tab[i];
		if (in_ss.fail())
			return(false);
	}
	return(true);
}

bool SprawozdanieMenu::file_read_line(ifstream & file, int & edges)
{
	string s;
	getline(file, s);

	if (file.fail() || s.empty())
		return(false);

	istringstream in_ss(s);

	in_ss >> edges;
	if (in_ss.fail())
		return(false);

	return(true);
}
