#pragma once
#include <iostream>
#include "TSPSolution.h"
#include "Timer.h"
#include "Population.h"



using namespace std;

class GeneticAlgorithmTSP
{

	int **miasto;
	int liczbaMiast;
	int kryterium_stopu;
	int amountOfPopulation;
	float wspolczynnikMutacji;
	float wspolczynnikKrzyzownaia;
	int metodaMutacji;

	Population* population;

public:

	GeneticAlgorithmTSP(int** miasto, int liczbaMiast, int kryterium_stopu, int population, float wspolczynnikMutacji, float wspolczynnikKrzyzownaia, int metodaMutacji);
	~GeneticAlgorithmTSP();


	TSPSolution* searchSolution();
	

};

