#pragma once
#include <windows.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
//#include "TabuSearch.h"
//#include "SimulatedAnnealing.h"
//#include "TSPSolution.h"


using namespace std;

class SprawozdanieMenu
{
	int **miasto;
	int liczbaMiast;
	int kryterium_stopu;
	bool dywersyfikacja;
	int minimumOptimum;

public:
	SprawozdanieMenu();
	~SprawozdanieMenu();

	void resetCity();

	void menu();
	void wczytajZPliku(string file_name);
	bool czyPoprawnaDecyzja(int max, int decyzja);
	void symulawaneWyzarzanie();
	void tabuSerach();
	bool file_read_line(ifstream & file, int tab[], int size);
	bool file_read_line(ifstream & file, int &edges);
};

