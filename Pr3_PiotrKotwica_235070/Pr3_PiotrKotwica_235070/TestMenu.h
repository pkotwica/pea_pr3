#pragma once
#include <windows.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include "GeneticAlgorithmTSP.h"
#include "TSPSolution.h"


using namespace std;

class TestMenu
{
	int **miasto;
	int liczbaMiast;
	int kryterium_stopu;
	int minimumOptimum;
	int population;
	float wspolczynnikMutacji;
	float wspolczynnikKrzyzownaia;
	int metoda_mutacji; //1-transpozycja, 2-inwersja

public:
	TestMenu();
	~TestMenu();

	void resetCity();

	void menu();
	void wczytajZPliku(string file_name);
	bool file_read_line(ifstream & file, int tab[], int size);
	bool file_read_line(ifstream & file, int &edges);
	void displayCity();
	bool czyPoprawnaDecyzja(int max, int decyzja);
	long long int read_QPC();
	void alorytmGenetyczny();
};

