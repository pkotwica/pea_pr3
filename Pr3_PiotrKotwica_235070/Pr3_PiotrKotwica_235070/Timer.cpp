#include "pch.h"
#include "Timer.h"


long long int Timer::read_QPC()
{
	LARGE_INTEGER count;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&count);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return((long long int)count.QuadPart);
}

Timer::Timer()
{
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);	startTime = 0;
	elapsed = 0;
}


Timer::~Timer()
{
}

void Timer::start()
{
	startTime = read_QPC();
}

void Timer::stop()
{
}

bool Timer::stop(float time)
{
	elapsed = read_QPC() - startTime;

	float stopTimeSeconds = (float)elapsed / frequency;

	if (stopTimeSeconds > time)
		return true;
	else
		return false;
}

double Timer::getTimeSeconds()
{
	return 0.0;
}
